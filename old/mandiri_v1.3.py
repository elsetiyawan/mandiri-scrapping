#!/usr/bin/env python

import os
import time
import mysql.connector
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


###############################################################################################
# KONEKSI DB
mydb = mysql.connector.connect(
	host = 'localhost',
	user = 'elsetiyawan',
	passwd = 'Phi227314!',
	database = 'rekening'
	)

mycursor = mydb.cursor()

# KUMPULAN FUNCTION 
def prosesrecord(hitung,tanggal,keterangan,debet,kredit):
	explode = tanggal.split('/')
	tgl = explode[2]+"-"+explode[1]+"-"+explode[0]
	id_murek = explode[2]+explode[1]+explode[0]+str(hitung).zfill(2) 
	query = "SELECT * FROM mandiri_murek WHERE id_murek LIKE '"+id_murek+"' AND tgl_murek LIKE '"+tanggal+"' AND ket_murek LIKE '"+keterangan+"' AND deb_murek LIKE '"+debet+"'  AND kre_murek LIKE '"+kredit+"' AND date_murek LIKE '"+tgl+"'"
	mycursor.execute(query)
	myresult = mycursor.fetchall()
	hitung = len(myresult)
	if hitung == 0:
		# now = datetime.now().strftime("%Y%m%d%H%M%S%s%f")
		insert = "INSERT INTO mandiri_murek (id_murek,tgl_murek,ket_murek,deb_murek,kre_murek,date_murek) VALUES ('"+id_murek+"','"+tanggal+"','"+keterangan+"','"+debet+"','"+kredit+"','"+tgl+"')"
		mycursor.execute(insert)
		mydb.commit()
		
def simpansaldo(saldo):
	id_bal = datetime.now().strftime("%Y%m%d%H%M%S%s%f")
	time = datetime.now().strftime("%Y%m%d%H%M%S")
	query = "INSERT INTO mandiri_balance (id_bal,date_bal,value_bal) VALUES ('"+id_bal+"','"+time+"','"+saldo+"')"
	mycursor.execute(query)
	mydb.commit()

def cekdb(tanggal):
	query = "SELECT * FROM mandiri_murek WHERE date_murek LIKE '"+tanggal+"'"
	mycursor.execute(query)
	mycursor.fetchall()
	return mycursor.rowcount

def cekstatus():
	query = "SELECT status FROM mandiri_status WHERE prosesor = 'mandiri'"
	mycursor.execute(query)
	status = mycursor.fetchone()
	return status[0]

###############################################################################################

while True:
	cek_status = cekstatus()
	if cek_status == "on":
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Sleeping.....")
		time.sleep(1)

		# buka browser
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Open browser")
		browser = webdriver.Chrome(os.getcwd()+'/chromedriver')

		# buka link
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Open website")
		simpan = browser.get('https://ib.bankmandiri.co.id')
		time.sleep(1)

		# masukkan username password dan click
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses login")
		username = browser.find_element_by_name('userID')
		password = browser.find_element_by_name('password')
		button = browser.find_element_by_name('image')
		# username.send_keys("setyawan87")
		# password.send_keys("131027")
		username.send_keys("PRAPIND001")
		password.send_keys("271013")
		button.click()
		time.sleep(1)

		# buka mutasi rekening
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses cek mutasi")
		browser.get('https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form')
		time.sleep(1)

		# pilih menu cek transaksi terakhir
		rekening = Select(browser.find_element_by_xpath("//select[@name='fromAccountID']"))
		# rekening.select_by_value('20070403087333')
		rekening.select_by_value('20161010054871')
		submit = browser.find_element_by_id('buttonsubmit')
		submit.click()
		time.sleep(1)

		# saldo akhir
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Cek saldo terakhir")
		saldo = browser.find_element_by_xpath("//table[4]/tbody/tr[5]/td[3]")
		balance = saldo.text
		simpansaldo(balance)
		time.sleep(1)
		

		# ambil data rekening
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses record data mutasi")
		get_mutasi = browser.find_elements_by_xpath("//table[3]/tbody/tr")
		count = len(get_mutasi)
		hitung = 1
		while(count > 1):
			tanggal = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[1]")
			keterangan = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[2]")
			debet = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[3]")
			kredit = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[4]")
			tgl = tanggal.text
			ket = keterangan.text
			deb = debet.text
			kre = kredit.text
			prosesrecord(hitung,tgl,ket,deb,kre)
			hitung += 1
			count -= 1

		# logout
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses logout")
		browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
		time.sleep(1)

		# tutup browser
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Tutup browser")
		browser.close()
		print("="*20)

	time.sleep(180)
