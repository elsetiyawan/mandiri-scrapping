import os
import time
import pymysql

from decimal import Decimal
from datetime import datetime
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

##################################################################################

conn = pymysql.connect(
	host='localhost',
	port=int('3306'),
	user='elsetiyawan',
	passwd='Phi227314!',
	db='rekening',
	charset='utf8mb4')

with conn:
	cur = conn.cursor()
	conn.autocommit(True)

def cek_status():
	cur.execute("SELECT status FROM status WHERE prosesor = 'mandiri'")
	fetch = cur.fetchone()
	return fetch[0]

def prosesrecord(hitung,tanggal,keterangan,debet,kredit,balance):
    explode = tanggal.split('/')
    tgl = explode[2]+"-"+explode[1]+"-"+explode[0]
    id_murek = explode[2]+explode[1]+explode[0]+str(hitung).zfill(2)
    query = "SELECT * FROM mandiri_murek WHERE id_murek LIKE '"+id_murek+"' AND ket_murek LIKE '"+keterangan+"' AND deb_murek LIKE '"+debet+"' AND kre_murek LIKE '"+kredit+"' AND date_murek LIKE '"+tgl+"' AND bal_murek LIKE '"+balance+"'"
    cur.execute(query)
    result = cur.fetchall()
    hitung = len(result)
    if hitung == 0:
        insert = "INSERT INTO mandiri_murek (id_murek,tgl_murek,ket_murek,deb_murek,kre_murek,date_murek,bal_murek) VALUES ('"+id_murek+"','"+tanggal+"','"+keterangan+"','"+debet+"','"+kredit+"','"+tgl+"','"+balance+"')"
        cur.execute(insert)

def simpansaldo(saldo):
    id_bal = datetime.now().strftime("%Y%m%d%H%M%S%s%f")
    time = datetime.now().strftime("%Y%m%d%H%M%S")
    query = "INSERT INTO mandiri_balance (id_bal,date_bal,value_bal) VALUES ('"+id_bal+"','"+time+"','"+saldo+"')"
    cur.execute(query)
####################################################################################

while True:
	display = Display(visible=0, size=(800,600))
	display.start()
	print("="*30)
	cekstatus = cek_status()
	if cekstatus == 'on':
		try:
			# buka browser dan url
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Opening Browser")
			dir = os.getcwd()
			browser = webdriver.Chrome(dir+'/chromedriver')
			time.sleep(1)
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Open website")
			browser.get('https://ib.bankmandiri.co.id')
			time.sleep(1)

			# login
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses login")
			username = browser.find_element_by_name('userID')
			password = browser.find_element_by_name('password')
			button = browser.find_element_by_name('image')
			username.send_keys("PRAPIND001")
			password.send_keys("271013")
			# username.send_keys("setyawan87")
			# password.send_keys("131027")
			time.sleep(1)
			button.click()

			# pilih menu mutasi
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Cek data mutasi")
			browser.get('https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form')
			select = Select(browser.find_element_by_xpath("//select[@name='fromAccountID']"))
			select.select_by_value('20161010054871')
			# select.select_by_value('20070403087333')
			submit = browser.find_element_by_id('buttonsubmit')
			time.sleep(1)
			submit.click()

			# record saldo akhir
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Record saldo akhir")
			saldoakhir = browser.find_element_by_xpath("//table[4]/tbody/tr[5]/td[3]")
			saldoawal = browser.find_element_by_xpath("//table[4]/tbody/tr[2]/td[3]")
			akhir = saldoakhir.text
			explode_akhir = akhir.split(".")
			saldoakhir = ''.join(explode_akhir)
			saldoakhir = saldoakhir.replace(',','.')
			awal = saldoawal.text
			explode_awal = awal.split(".")
			saldoawal = ''.join(explode_awal)
			saldoawal = saldoawal.replace(',','.')
			simpansaldo(saldoakhir)
			time.sleep(1)

			# get data mutasi
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Record data mutasi")
			get_mutasi = browser.find_elements_by_xpath("//table[3]/tbody/tr")
			count = len(get_mutasi)
			hitung = 1
			balance = Decimal(saldoawal)
			while(count > 1):
			    mutasi = []
			    tanggal = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[1]")
			    keterangan = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[2]")
			    debet_ori = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[3]")
			    debet_replace = debet_ori.text.replace('.','')
			    debet = debet_replace.replace(',','.')
			    kredit_ori = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[4]")
			    kredit_replace = kredit_ori.text.replace('.','')
			    kredit = kredit_replace.replace(',','.')
			    balance = Decimal(balance) - Decimal(debet) + Decimal(kredit)

			    prosesrecord(hitung,tanggal.text,keterangan.text,debet,kredit,str(balance))
			    hitung += 1
			    count -= 1
			time.sleep(1)

			# logout
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Logout dan tutup browser")
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Sleeping for 3 minutes")
			browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
			browser.close()
			time.sleep(180)
		except:
			print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses failed!")
			browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
			browser.close()
			time.sleep(60)
	else:
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses OFF!")
		time.sleep(30)
