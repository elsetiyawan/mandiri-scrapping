import os
import time
import pymysql

from decimal import Decimal
from datetime import datetime,timedelta
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

##################################################################################

def koneklokal():
	connlokal = pymysql.connect(
		host='localhost',
		port=int('3306'),
		user='elsetiyawan',
		passwd='Phi227314!',
		db='rekening',
		charset='utf8mb4')
	with connlokal:
		curlokal = connlokal.cursor()
		connlokal.autocommit(True)
	return curlokal

def cek_status():
	curlokal = koneklokal()
	curlokal.execute("SELECT status FROM status WHERE prosesor = 'mandiri'")
	fetch = curlokal.fetchone()
	return fetch[0]
	curlokal.close()

def prosesrecord(hitung,tanggal,keterangan,debet,kredit,balance):
	curlokal = koneklokal()
	explode = tanggal.split('/')
	tgl = explode[2]+"-"+explode[1]+"-"+explode[0]
	id_murek = explode[2]+explode[1]+explode[0]+str(hitung).zfill(2)
	query = "SELECT * FROM mandiri_murek WHERE id_murek LIKE '"+id_murek+"' AND ket_murek LIKE '"+keterangan+"' AND deb_murek LIKE '"+debet+"' AND kre_murek LIKE '"+kredit+"' AND date_murek LIKE '"+tgl+"' AND bal_murek LIKE '"+balance+"'"
	curlokal.execute(query)
	result = curlokal.fetchall()
	hitung_result = len(result)
	if hitung_result == 0:
		insert = "INSERT INTO mandiri_murek (id_murek,row_murek,tgl_murek,ket_murek,deb_murek,kre_murek,date_murek,bal_murek,status) VALUES ('"+id_murek+"','"+str(hitung)+"','"+tanggal+"','"+keterangan+"','"+debet+"','"+kredit+"','"+tgl+"','"+balance+"','q')"
		curlokal.execute(insert)
	curlokal.close()


def simpansaldo(saldo):
	curlokal = koneklokal()
	id_bal = datetime.now().strftime("%Y%m%d%H%M%S%s%f")
	time = datetime.now().strftime("%Y%m%d%H%M%S")
	query = "INSERT INTO mandiri_balance (id_bal,date_bal,value_bal) VALUES ('"+id_bal+"','"+time+"','"+saldo+"')"
	curlokal.execute(query)
	curlokal.close()

####################################################################################

while True:
	# tentukan jam tidak aktif
	deactive_time = ['22','23','24','00','01','03','04','05']
	jam = datetime.now().strftime("%H")
	if str(jam) in deactive_time:
		print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Jam non-aktif ")
		time.sleep(1800)

	else:
		if str(jam) == '02':
			try:
				# buka browser dan url
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Opening Browser")
				options = webdriver.ChromeOptions()
				options.add_argument('headless')
				options.add_argument("disable-gpu")
				browser = webdriver.Chrome(os.getcwd()+'/chromedriver', options=options)
				time.sleep(1)
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Open website")
				hitlink = browser.get('https://ib.bankmandiri.co.id')
				time.sleep(1)

				# login
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses login")
				username = browser.find_element_by_name('userID')
				password = browser.find_element_by_name('password')
				button = browser.find_element_by_name('image')
				username.send_keys("PRAPIND001")
				password.send_keys("271013")
				time.sleep(1)
				button.click()

				# cek saldo akhir
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Record saldo akhir")
				hitlink = browser.get('https://ib.bankmandiri.co.id/retail/AccountDetail.do?action=result&ACCOUNTID=20161010054871')
				saldoakhir = browser.find_element_by_id('accbal').text
				saldoakhir = saldoakhir.replace('Rp. ','').replace('.','').replace(',','.')
				simpansaldo(saldoakhir)
				time.sleep(5)

				# pilih menu mutasi
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Cek data mutasi")
				hitlink = browser.get('https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form')
				kemarin = datetime.now() - timedelta(days = 1)
				thn = kemarin.strftime("%Y")
				bln = kemarin.strftime("%-m")
				hr = kemarin.strftime("%-d")
				rekening = Select(browser.find_element_by_xpath("//select[@name='fromAccountID']"))
				tgl_awal = Select(browser.find_element_by_xpath("//select[@name='fromDay']"))
				bln_awal = Select(browser.find_element_by_xpath("//select[@name='fromMonth']"))
				thn_awal = Select(browser.find_element_by_xpath("//select[@name='fromYear']"))
				tgl_akhir = Select(browser.find_element_by_xpath("//select[@name='toDay']"))
				bln_akhir = Select(browser.find_element_by_xpath("//select[@name='toMonth']"))
				thn_akhir = Select(browser.find_element_by_xpath("//select[@name='toYear']"))
				submit = browser.find_element_by_id('buttonsubmit')

				rekening.select_by_value('20161010054871')
				tgl_awal.select_by_value(hr)
				bln_awal.select_by_value(bln)
				thn_awal.select_by_value(thn)
				tgl_akhir.select_by_value(hr)
				bln_akhir.select_by_value(bln)
				thn_akhir.select_by_value(thn)
				time.sleep(1)
				submit.click()

				# saldo awal
				saldoawal = browser.find_element_by_xpath("//table[4]/tbody/tr[2]/td[3]")
				awal = saldoawal.text
				explode_awal = awal.split(".")
				saldoawal = ''.join(explode_awal)
				saldoawal = saldoawal.replace(',','.')
				time.sleep(1)

				# get data mutasi
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Record data mutasi")
				get_mutasi = browser.find_elements_by_xpath("//table[3]/tbody/tr")
				count = len(get_mutasi)
				hitung = 1
				balance = Decimal(saldoawal)
				while(count > 1):
					mutasi = []
					tanggal = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[1]")
					keterangan = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[2]")
					debet_ori = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[3]")
					debet_replace = debet_ori.text.replace('.','')
					debet = debet_replace.replace(',','.')
					kredit_ori = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[4]")
					kredit_replace = kredit_ori.text.replace('.','')
					kredit = kredit_replace.replace(',','.')
					balance = Decimal(balance) - Decimal(debet) + Decimal(kredit)

					prosesrecord(hitung,tanggal.text,keterangan.text,debet,kredit,str(balance))
					hitung += 1
					count -= 1
					time.sleep(1)

				# logout
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Logout dan tutup browser")
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Sleeping for 1 minutes")
				print("="*30)
				hitlink = browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
				browser.quit()
				time.sleep(60)
			except:
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses failed!")
				hitlink = browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
				print("="*30)
				browser.quit()
				time.sleep(60)

		else:
			cekstatus = cek_status()
			if cekstatus == 'on':
				try:
					# buka browser dan url
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Opening Browser")
					options = webdriver.ChromeOptions()
					options.add_argument('headless')
					options.add_argument("disable-gpu")
					browser = webdriver.Chrome(os.getcwd()+'/chromedriver', options=options)
					time.sleep(1)
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Open website")
					hitlink = browser.get('https://ib.bankmandiri.co.id')
					time.sleep(1)

					# login
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses login")
					username = browser.find_element_by_name('userID')
					password = browser.find_element_by_name('password')
					button = browser.find_element_by_name('image')
					username.send_keys("PRAPIND001")
					password.send_keys("271013")
					time.sleep(1)
					button.click()

					# cek saldo akhir
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Record saldo akhir")
					hitlink = browser.get('https://ib.bankmandiri.co.id/retail/AccountDetail.do?action=result&ACCOUNTID=20161010054871')
					saldoakhir = browser.find_element_by_id('accbal').text
					saldoakhir = saldoakhir.replace('Rp. ','').replace('.','').replace(',','.')
					simpansaldo(saldoakhir)
					time.sleep(5)

					# pilih menu mutasi
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Cek data mutasi")
					hitlink = browser.get('https://ib.bankmandiri.co.id/retail/TrxHistoryInq.do?action=form')
					select = Select(browser.find_element_by_xpath("//select[@name='fromAccountID']"))
					select.select_by_value('20161010054871')
					submit = browser.find_element_by_id('buttonsubmit')
					time.sleep(1)
					submit.click()

					# saldo awal
					saldoawal = browser.find_element_by_xpath("//table[4]/tbody/tr[2]/td[3]")
					awal = saldoawal.text
					explode_awal = awal.split(".")
					saldoawal = ''.join(explode_awal)
					saldoawal = saldoawal.replace(',','.')
					time.sleep(1)

					# get data mutasi
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Record data mutasi")
					get_mutasi = browser.find_elements_by_xpath("//table[3]/tbody/tr")
					count = len(get_mutasi)
					hitung = 1
					balance = Decimal(saldoawal)
					while(count > 1):
						mutasi = []
						tanggal = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[1]")
						keterangan = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[2]")
						debet_ori = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[3]")
						debet_replace = debet_ori.text.replace('.','')
						debet = debet_replace.replace(',','.')
						kredit_ori = browser.find_element_by_xpath("//table[3]/tbody/tr["+str(count)+"]/td[4]")
						kredit_replace = kredit_ori.text.replace('.','')
						kredit = kredit_replace.replace(',','.')
						balance = Decimal(balance) - Decimal(debet) + Decimal(kredit)

						prosesrecord(hitung,tanggal.text,keterangan.text,debet,kredit,str(balance))
						hitung += 1
						count -= 1
						time.sleep(1)

					# logout
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Logout dan tutup browser")
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Sleeping for 1 minutes")
					print("="*30)
					hitlink = browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
					browser.quit()
					time.sleep(60)

				except:
					print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses failed!")
					hitlink = browser.get('https://ib.bankmandiri.co.id/retail/Logout.do?action=result')
					print("="*30)
					browser.quit()
					time.sleep(60)

			else:
				print(datetime.now().strftime("%Y-%m-%d %H:%M:%S")+" ==> Proses OFF!")
				print("="*30)
				time.sleep(30)
